import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Mixin from './minis/index'
import Message from './components/Message/Message'

Vue.config.productionTip = false
Vue.mixin(Mixin)
Vue.use(Message)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
