import Message from './Message.vue'
let MessageObj = {}
// install 注册全局组件
MessageObj.install = function(Vue){
    // 是一种局部注册
    // extend创建的是一个组件构造器，而不是一个具体的组件实例

    //     所以他不能直接在new Vue中这样使用： new Vue({components: first})
    
    //    最终还是要通过Vue.components注册才可以使用的。
    let MyMessage = Vue.extend(Message) //这是一个类似于组件实例的一个组件构造器
 
    let $vm = new MyMessage() //组件实例

    let vNode = $vm.$mount().$el

    document.body.appendChild(vNode)
    Vue.prototype.$Message=(msg)=>{
        $vm.flag=true
        $vm.WordData=msg.message
        console.log($vm.WordData)
        if(typeof msg==='string'){
      $vm.WordData=msg.message
        }else{
             $vm.WordData=msg.message
            switch(msg.type){
                case 'success':
                    $vm.className='success'
                break
                case 'babi':
                    $vm.className='babi'
                break
            }
        }
        
        setTimeout(()=>{
        $vm.flag=false
        },$vm.time)
        
    }
    console.log()
}
export default MessageObj