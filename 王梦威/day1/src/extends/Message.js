import Message from '../extends/Message.vue'
let MessageObj={}
//必须install，写别的就无法全局使用
MessageObj.install=function(Vue){
    let Moban=Vue.extend(Message)
    //组件实例  自己的模板
    let $Dom=new Moban() 
    //找到dom解构
    let $Node=$Dom.$mount().$el
    //添加到Home页面
    document.body.appendChild($Node)

     //Vue.prototype是在原型上定义，使其在每个Vue中的实例中可用
    Vue.prototype.$message=(msg)=>{
        $Dom.flag=true    //自己在模板写的参数
        $Dom.content=msg.content
        //也可自己在根据条件修改
        setTimeout(()=>{
            $Dom.flag=false
        },3000)
        switch(msg.type){
            case 'yes':
                $Dom.className='yes'
                break
            case 'no':
                $Dom.className='no'
                break
            case 'mes':
                $Dom.className='mes'
                break
            case 'error':
                $Dom.className='error'
                break
        }
    }
}

//抛出
export default MessageObj
