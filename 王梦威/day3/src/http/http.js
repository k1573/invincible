import axios from 'axios'
import Nprogress from 'nprogress'
import Cookies from 'js-cookie'
axios.interceptors.request.use((config)=>{
    const token=Cookies.get('token')
    config.headers.authorization=token
    Nprogress.start()
    return config
})

axios.interceptors.response.use((data)=>{
    Nprogress.done()
    return data.data
})

export default axios