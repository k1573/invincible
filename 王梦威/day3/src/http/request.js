import axios from './http'

export const getUrl=(url,params)=>{
    return axios.get(url,{params})
}

export const postUrl=(url,params)=>{
    return axios.post(url,params)
}

export const deleteUrl=(url,params)=>{
    return axios.delete(url,{params})
}

export const deleteUrlData=(url,params)=>{
    return axios.delete(url,{data:params})
}

export const putUrl=(url,params)=>{
    return axios.put(url,params)
}