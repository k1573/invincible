export default[
    { 
    path:'/home',
    name:'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/home/index.vue'),
    children:[
        {
            path:"/home/questionsType",
            name:"questionsType",
            component: () => import(/* webpackChunkName: "questionsType" */ '../views/home/questionsType.vue')
        },{
            path:"/home/addQuestions",
            name:"addQuestions",
            component: () => import(/* webpackChunkName: "addQuestions" */ '../views/home/addQuestions.vue')
        },{
            path:"/home/watchQuestions",
            name:"watchQuestions",
            component: () => import(/* webpackChunkName: "watchQuestions" */ '../views/home/watchQuestions.vue')
        }
    ]
}
]