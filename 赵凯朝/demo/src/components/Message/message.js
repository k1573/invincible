import Message from './Message.vue';
let MessageObj = {};
MessageObj.install = (Vue) => {
    //创建子类
    let myMessage = Vue.extend(Message);
    //实例化
    let $vm = new myMessage();
    //找到dom
    let vDom = $vm.$mount().$el;
    //添加到页面
    document.body.appendChild(vDom);
    //挂载实例
    Vue.prototype.$message = (msg, time = 1500) => {
        //判断参数
        if (typeof msg === 'string') {
            $vm.flag = true;
            $vm.text = msg;
            $vm.type = '';
        } else {
            $vm.flag = true;
            $vm.text = msg.text;
            switch (msg.type) {
                //成功
                case "success":
                    $vm.type = 'message-success'
                    break;
                    //警告
                case "warning":
                    $vm.type = 'message-warning'
                    break;
                    //错误
                case "error":
                    $vm.type = 'message-error'
                    break;
                default:
                    $vm.type = ''
            }
        }
        //隐藏
        setTimeout(() => {
            $vm.flag = false;
        }, time)
    }

    // Vue.prototype.$message = {
    //     error(msg) {
    //         $vm.flag = true;
    //         $vm.text = msg;
    //         $vm.type = 'message-error';
    //         setTimeout(() => {
    //             $vm.flag = false;
    //         }, 1500)
    //     }
    // }
}

export default MessageObj;