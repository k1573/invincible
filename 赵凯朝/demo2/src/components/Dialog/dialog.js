import Dialog from './Dialog.vue';
let DialogObj = {};
DialogObj.install = (Vue) => {
    //创建子类
    let myDialog = Vue.extend(Dialog);
    //实例化
    let $vm = new myDialog();
    //获取dom
    let vDom = $vm.$mount().$el;
    //添加到页面上
    document.body.appendChild(vDom);
    //挂载实例
    Vue.prototype.$confirm = (msg) => {
        // $vm.title = msg.title;
        // $vm.text = msg.text;
        // $vm.flag = true;
        return new Promise((reslove, reject) => {
            if (!msg) reject(msg)
            reslove($vm.title = msg.title,
                $vm.text = msg.text,
                $vm.flag = true)
        }).then(result => {
            console.log(result)
        }).catch(error => {
            console.log(error)
        })
    }
}

export default DialogObj;