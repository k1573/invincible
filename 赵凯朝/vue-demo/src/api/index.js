import { getUrl, postUrl, putUrl } from '../http/request';
//获取所有试题类型
export const getQuestionType = () => {
    return getUrl('/exam/getQuestionsType');
};
//添加试题类型
export const addQuestionType = (data) => {
    return getUrl('/exam/insertQuestionsType', data);
};

//删除指定的试题类型
export const delQuestionsType = (data) => {
    return postUrl('/exam/delQuestionsType', data);
};

//获取所有的课程
export const getSubject = () => {
    return getUrl('/exam/subject');
}

//获取所有的考试类型
export const getExamType = () => {
    return getUrl('/exam/examType');
}

//添加试题接口
export const addQuestions = (data) => {
    return postUrl('/exam/questions', data);
};

//获取所有的试题
export const getQuestions = () => {
    return getUrl('/questions/new');
}

//按条件获取试题
export const getQuestionsCond = (data) => {
    return getUrl('/exam/questions/condition', data);
};

//更新试题
export const updateQuestions = (data) => {
    return putUrl('/exam/update', data);
};

//添加用户
export const addUser = (data) => {
    return postUrl('/user', data);
};

//添加身份
export const addIdentity = (data) => {
    return getUrl('/user/identity/edit', data);
};

//添加视图权限
export const addAuthorityView = (data) => {
    return getUrl('/user/authorityView/edit', data);
};

//添加api接口权限
export const addAuthorityApi = (data) => {
    return getUrl('/user/authorityApi/edit', data);
};

//更新用户信息(用户名，用户密码，用户身份)
export const updateUser = (data) => {
    return putUrl('/user/user', data);
};

//给身份设定api接口权限
export const setIdentityApi = (data) => {
    return postUrl('/user/setIdentityApi', data);
};

//展示用户数据
export const getUser = () => {
    return getUrl('/user/user');
}

//展示身份数据
export const getIdentity = () => {
    return getUrl('/user/identity');
};
//展示api接口权限数据
export const getAuthorApi = () => {
    return getUrl('/user_authority');
}

//展示身份和api权限关系
export const getAuthorApiRelation = () => {
    return getUrl('/user/identity_api_authority_relation');
}

//登录接口
export const login = (data) => {
    return postUrl('/user/login', data);
};

//获取当前用户信息
export const getUserInfo = () => {
    return getUrl('/user/userInfo');
}