import axios from 'axios';
import Cookies from 'js-cookie'
import nprogress from 'nprogress'
axios.interceptors.request.use(config => {
    config.url = 'http://127.0.0.1:7001' + config.url;
    let token = Cookies.get('token');
    config.headers.Authorization = token;
    nprogress.start();
    return config;
})
axios.interceptors.response.use(data => {
    nprogress.done();
    return data.data
})
export default axios;