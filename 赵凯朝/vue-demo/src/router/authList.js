import main from '../views/home/index.vue';
export default [{
    path: '/main',
    name: 'main',
    component: main,
    redirect: '/home/testManage',
    meta: {
        auth: ['管理员', '出题者', '浏览者']
    },
    children: [{
        path: '/home/testManage',
        name: 'testManage',
        component: () =>
            import ( /* webpackChunkName: "testManage" */ '../views/home/testManage/index.vue'),
        meta: {
            auth: ['管理员', '出题者', '浏览者']
        },
        title: '试题管理',
        redirect: '/home/testManage/addQuestions',
        children: [{
            path: '/home/testManage/addQuestions',
            name: 'addQuestions',
            component: () =>
                import ( /* webpackChunkName: "addQuestions" */ '../views/home/testManage/addQuestions.vue'),
            title: '添加试题',
            meta: {
                auth: ['管理员', '出题者']
            },
        }, {
            path: '/home/testManage/questionsType',
            name: 'questionsType',
            component: () =>
                import ( /* webpackChunkName: "questionsType" */ '../views/home/testManage/questionsType.vue'),
            meta: {
                auth: ['管理员']
            },
            title: '试题分类'
        }, {
            path: '/home/testManage/watchQuestions',
            name: 'watchQuestions',
            component: () =>
                import ( /* webpackChunkName: "watchQuestions" */ '../views/home/testManage/watchQuestions.vue'),
            meta: {
                auth: ['管理员', '浏览者', '出题者']
            },
            title: '查看试题'
        }]
    }, ]
}]