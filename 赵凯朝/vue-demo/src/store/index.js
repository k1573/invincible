import Vue from 'vue'
import Vuex from 'vuex'
import AuthList from './modules/router'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        AuthList
    }
})