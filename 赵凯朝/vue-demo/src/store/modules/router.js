export default {
    //开启命名空间
    namespaced: true,
    state: {
        authList: []
    },
    mutations: {
        changeAuthList(state, actions) {
            state.authList = actions.list
        }
    },
    actions: {},
    modules: {}
}