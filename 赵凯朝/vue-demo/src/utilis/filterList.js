export const filterRoutes = (arr, auth) => {
    const res = arr.filter(item => {
        return item.meta.auth.indexOf(auth) !== -1
    })
    res.length > 0 && res.forEach(item => {
        if (item.children) {
            item.children = filterRoutes(item.children, auth)
        }
    })
    return res
}